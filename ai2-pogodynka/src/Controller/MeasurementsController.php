<?php

namespace App\Controller;

use App\Entity\Weather;
use App\Form\WeatherType;
use App\Repository\WeatherRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MeasurementsController extends AbstractController
{
    public function index(WeatherRepository $weatherRepository): Response
    {
        return $this->render('measurements/index.html.twig', [
            'weather' => $weatherRepository->findAll(),
        ]);
    }

    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_NEW_MEASUREMENT');

        $weather = new Weather();
        $form = $this->createForm(WeatherType::class, $weather, [
            'validation_groups' => 'add_measurement'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($weather);
            $entityManager->flush();
            $this->addFlash('success', 'Measurement added!');
            return $this->redirectToRoute('measurements_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('measurements/new.html.twig', [
            'weather' => $weather,
            'form' => $form->createView(),
        ]);
    }

    public function show(Weather $weather): Response
    {
        return $this->render('measurements/show.html.twig', [
            'weather' => $weather,
        ]);
    }

    public function edit(Request $request, Weather $weather): Response
    {
        $this->denyAccessUnlessGranted('ROLE_EDIT_MEASUREMENT');

        $form = $this->createForm(WeatherType::class, $weather, [
            'validation_groups' => 'edit_measurement'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Measurement edited!');
            return $this->redirectToRoute('measurements_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('measurements/edit.html.twig', [
            'weather' => $weather,
            'form' => $form->createView(),
        ]);
    }

    public function delete(Request $request, Weather $weather): Response
    {
        $this->denyAccessUnlessGranted('ROLE_DELETE_MEASUREMENT');

        if ($this->isCsrfTokenValid('delete'.$weather->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($weather);
            $entityManager->flush();
        }
        $this->addFlash('danger', 'Measurement deleted irrevocably!');
        return $this->redirectToRoute('measurements_index', [], Response::HTTP_SEE_OTHER);
    }
}
