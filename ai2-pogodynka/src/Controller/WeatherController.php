<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Location;
use App\Repository\WeatherRepository;
use App\Repository\LocationRepository;
use App\Service\WeatherUtil;

class WeatherController extends AbstractController
{
    public function cityAction(Location $city, WeatherRepository $weatherRepository): Response
    {
        $weathers = $weatherRepository->findByLocation($city);
        
        return $this->render('weather/city.html.twig', [
            'location' => $city,
            'weathers' => $weathers,
        ]);
    }
    /*
    public function country_cityAction(WeatherRepository $weatherRepository, LocationRepository $locationRepository, string $country, string $city): Response
    {
        $weather_in_city = $locationRepository->findOneBy(['name'=>$city, 'country'=>$country]);
        $weathers = $weatherRepository->findByLocation($weather_in_city);
        
        return $this->render('weather/city.html.twig', [
            'location' => $weather_in_city,
            'weathers' => $weathers,
        ]);
       


    }*/
     //zamieniam na serwis
    public function country_cityAction(WeatherUtil $weatherUtil, WeatherRepository $weatherRepository, LocationRepository $locationRepository, string $country, string $city): Response
    {
        $weather_in_city = $weatherUtil->getWeatherForCountryAndCity($country, $city, $weatherRepository, $locationRepository);

        
        return $this->render('weather/city.html.twig', [
            'location' => $weather_in_city['location'],
            'weathers' => $weather_in_city['measurement'],
        ]);
       


    }

    public function city_nameAction(WeatherRepository $weatherRepository, LocationRepository $locationRepository, string $city): Response
    {
        $weather_in_city = $locationRepository->findOneBy(['name'=>$city]);
        $weathers = $weatherRepository->findByLocation($weather_in_city);
        return $this->render('weather/city.html.twig', [
            'location' => $weather_in_city,
            'weathers' => $weathers,
        ]);
    }

    public function main(): Response
    {
        return $this->render('weather/index.html.twig');
    }
}
