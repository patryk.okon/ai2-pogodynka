<?php

namespace App\Service;

use App\Repository\LocationRepository;
use App\Repository\WeatherRepository;
use App\Entity\Location;

class WeatherUtil{
    public function getWeatherForLocation(Location $city, WeatherRepository $weatherRepository): array{
        $arr_measurements = $weatherRepository->findByLocation($city);
        return $arr_measurements;
    }

    public function getWeatherForCountryAndCity(string $countryCode, string $cityName, WeatherRepository $weatherRepository, LocationRepository $locationRepository): ?array{
        $city = $locationRepository->findOneBy(['country'=>$countryCode, 'name'=>$cityName]);
        
        if($city == null){
            return [];
        }else{
            return ['location'=>$city,
                    'measurement'=>$this->getWeatherForLocation($city, $weatherRepository)];
        }
    } 
}