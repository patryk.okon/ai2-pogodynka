<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use App\Service\WeatherUtil;

#[AsCommand(
    name: 'CommandGetWeatherById',
    description: 'Add a short description for your command',
)]
class CommandGetWeatherByIdCommand extends Command
{
    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $containerInterface, WeatherUtil $weatherUtil){
        $this->entityManager = $entityManager;
        $this->containerInterface = $containerInterface;

        $this->weatherUtil = $weatherUtil;    

        parent::__construct();    
    }

    protected function configure(): void{
        $this->addArgument('argument', InputArgument::REQUIRED, 'Location ID');
    }

    protected function execute(InputInterface $inputInterface, OutputInterface $outputInterface): int{
        $interface = new SymfonyStyle($inputInterface, $outputInterface);
        $argument = $inputInterface->getArgument('argument');

        $numberLocation = (int)$argument;
        $location=$this->containerInterface->get('doctrine')->getRepository('App:Location');
        $measurement=$this->containerInterface->get('doctrine')->getRepository('App:Weather');
        
        $searchLocation = $location->find($numberLocation);

        if($searchLocation==null){
            $interface->error("Miasto o takim id nie istnieje w bazie danych :(");
            return 1;
        }
        $result=$this->weatherUtil->getWeatherForLocation($searchLocation, $measurement);
        $interface->success("Temperatura w tym mieście wynosi ".$result[0]->getTemperature()." stopni.");
        return 0;
    }
}
