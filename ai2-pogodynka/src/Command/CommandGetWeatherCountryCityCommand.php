<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use App\Service\WeatherUtil;

#[AsCommand(
    name: 'CommandGetWeatherCountryCity:command',
    description: 'Add a short description for your command',
)]
class CommandGetWeatherCountryCityCommand extends Command
{
    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $containerInterface, WeatherUtil $weatherUtil){
        $this->entityManager = $entityManager;
        $this->containerInterface = $containerInterface;

        $this->weatherUtil = $weatherUtil;    

        parent::__construct();    
    }

    protected function configure(): void{
        $this->addArgument('argument1', InputArgument::REQUIRED, 'Country');
        $this->addArgument('argument2', InputArgument::REQUIRED, 'City name');
    }

    protected function execute(InputInterface $inputInterface, OutputInterface $outputInterface): int{
        $interface = new SymfonyStyle($inputInterface, $outputInterface);
        $argument1 = $inputInterface->getArgument('argument1');
        $argument2 = $inputInterface->getArgument('argument2');

        $location=$this->containerInterface->get('doctrine')->getRepository('App:Location');
        $measurement=$this->containerInterface->get('doctrine')->getRepository('App:Weather');
        
        $searchLocation = $location->findBy(array('country'=>$argument1, 'name'=>$argument2));
        if($searchLocation==null){
            $interface->error("Miasto o takim kodzie kraju i nazwie nie istnieje w bazie danych :(");
            return 1;
        }
        $result=$this->weatherUtil->getWeatherForCountryAndCity($searchLocation[0]->getCountry(),$searchLocation[0]->getName(), $measurement, $location);
        $interface->success("Temperatura w tym mieście wynosi ".$result['measurement'][0]->getTemperature()." stopni.");
        return 0;
    }
}
